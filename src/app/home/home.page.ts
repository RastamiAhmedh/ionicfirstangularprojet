import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  isShowCard: boolean;
  constructor(private menuController: MenuController) {}

  showCard(){
    if(this.isShowCard)
    {
      this.isShowCard = false;
    }else {
      this.isShowCard = true;
    }
  }
  openFirst() {
    this.menuController.enable(true, 'first');
    this.menuController.open('first');
  }
  openEnd() {
    this.menuController.open('end');
  }

  openCustom() {
    this.menuController.enable(true, 'custom');
    this.menuController.open('custom');
  }
}
