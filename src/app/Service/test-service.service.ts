import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { User } from '../Model/user';
import { HttpClient } from '@angular/common/http';
import{retry,catchError}from 'rxjs/internal/operators'

@Injectable({
  providedIn: 'root'
})
export class TestServiceService {
 
  constructor(private http : HttpClient) { }
  getAllUser() : Observable<Array<User>>{
    return this.http.get<Array<User>>("http://localhost:3000/users").pipe(retry(1),catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = '';
    if ( error.error instanceof ErrorEvent ) {
    // Get client-side error
    errorMessage = error.error.message;
    } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
