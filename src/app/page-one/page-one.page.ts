import { Component, OnInit } from '@angular/core';
import { delay } from 'rxjs/internal/operators';
import { User } from 'src/app/Model/user';
import { TestServiceService } from 'src/app/Service/test-service.service';

@Component({
  selector: 'app-page-one',
  templateUrl: './page-one.page.html',
  styleUrls: ['./page-one.page.scss'],
})
export class PageOnePage implements OnInit {
  users : Array<User>
  isLoading : boolean;
  constructor(private userService: TestServiceService) { }
  
  ngOnInit() {
    this.isLoading = true;
    this.userService.getAllUser().subscribe((dataReceived =>{
      this.users = dataReceived;
     setTimeout(() => {this.isLoading = false;},3000)
      
    }))
  }

}
